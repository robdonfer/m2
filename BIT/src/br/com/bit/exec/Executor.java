package br.com.bit.exec;



public class Executor{


	public static int somarValorPosicao(int valores[][]) {
		
		int soma = 0;
		int somaParcial = 0;
		int posicao = 0; 
		for (int i = 0; i < valores.length; i++) {
			int posicaoEsq = 0;
			int posicaoDir = 0;
			somaParcial = 0;
				
			if( valores[i].length == 2){
				posicaoDir = posicao + 1;
			}else if(valores[i].length > 2){
				posicaoEsq = posicao;
				posicaoDir = posicao + 1;
			}	
				
			if(posicaoEsq == 0 && posicaoDir == 0){
				somaParcial = valores[i][posicaoEsq];
			
			}else if(posicaoEsq > 0 || posicaoDir > 0){
				int esq = valores[i][posicaoEsq];
				int dir = valores[i][posicaoDir];
					
				if(esq > dir){
					somaParcial = esq;
					posicao = posicaoEsq;
				}else{
					somaParcial = dir;
					posicao = posicaoDir;
				}
			}
			System.out.println("Valores selecionados para soma: "+ somaParcial);
			soma+= somaParcial;
		}
		return soma;
	}

}
